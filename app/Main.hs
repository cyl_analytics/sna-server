{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.Monoid ((<>))
import Control.Applicative
import Network.Wai
import Network.Wai.Handler.Warp
import Control.Monad.Trans.Either (EitherT, runEitherT, left, right)
import Control.Monad.Trans.Reader (ReaderT, runReaderT)
import Database.SQLite.Simple (Connection, open, execute_)
import Control.Monad.IO.Class (liftIO)
import System.Log.FastLogger (newFileLoggerSet, defaultBufSize)
import Options.Applicative
import Servant

import SNA.Facebook
import SNA.Types

readerToEither' :: SNAState -> (forall a. AppM a -> EitherT ServantErr IO a)
readerToEither' state ev = do
    ans <- liftIO $ runReaderT (runEitherT ev) state
    case ans of
        Left e -> left e
        Right r -> right r

readerToEither :: SNAState -> AppM :~> EitherT ServantErr IO
readerToEither state = Nat (readerToEither' state)


type SNAAPI = "v1" :> FacebookAPI


snaAPI :: Proxy SNAAPI
snaAPI = Proxy

serverT :: ServerT SNAAPI AppM
serverT = fbServer

server :: SNAState -> Server SNAAPI
server state = enter (readerToEither state) serverT


app :: SNAState -> Application
app state = serve snaAPI (server state)


data CrawlerArgs = CrawlerArgs
    { caDBPath :: FilePath
    , caLogPath :: FilePath
    }

crawlerArgs :: Parser CrawlerArgs
crawlerArgs =
    CrawlerArgs <$>
    strOption
        (long "dbpath" <> metavar "FILE" <> help "Path to SQLite db file") <*>
    strOption (long "logpath" <> metavar "FILE" <> help "Path to log folder")


opts =
    info
        (helper <*> crawlerArgs)
        (fullDesc <> progDesc "Server for register pages" <>
         header "sna-server - social network analysis server")



main :: IO ()
main = do
    ca <- execParser opts
    conn <- open (caDBPath ca)
    logger <- newFileLoggerSet defaultBufSize (caLogPath ca <> "/server.log")
    execute_
        conn
        ("CREATE TABLE IF NOT EXISTS fbPage " <>
         "(pid TEXT PRIMARY KEY, created_time Int, updated_time Int)")
    execute_
        conn
        ("CREATE TABLE IF NOT EXISTS fbUser " <>
         "(uid TEXT PRIMARY KEY, token TEXT, created_time Int, updated_time Int)")

    run 80 (app (SNAState conn logger))
